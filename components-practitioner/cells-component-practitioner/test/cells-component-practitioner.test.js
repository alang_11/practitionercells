import { html, fixture, assert, fixtureCleanup } from '@open-wc/testing';
import '../cells-component-practitioner.js';

suite('<cells-component-practitioner>', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<cells-component-practitioner></cells-component-practitioner>`);
    await el.updateComplete;
  });

  test('instantiating the element with default properties works', () => {
    const element = el.shadowRoot.querySelector('p');
    assert.equal(element.innerText, 'Welcome to Cells');
  });

});





