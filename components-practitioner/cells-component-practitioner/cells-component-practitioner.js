import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-component-practitioner-styles.js';
/**
This component ...

Example:

```html
<cells-component-practitioner></cells-component-practitioner>
```

##styling-doc

@customElement cells-component-practitioner
@polymer
@LitElement
@demo demo/index.html
*/
export class CellsComponentPractitioner extends LitElement {
  static get is() {
    return 'cells-component-practitioner';
  }

  // Declare properties
  static get properties() {
    return {
      name: { type: String, },
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.name = 'Cells';
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-component-practitioner-shared-styles')
    ]
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <p>Welcome to ${this.name}</p>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsComponentPractitioner.is, CellsComponentPractitioner);
